I made this to play with some modern HTML and CSS techniques, and to advocate for better sleep. I am not a doctor and none of this should be taken as medical advice.

I limited myself to a weekend to build this. If I had more time, I would have put more effort into the design, especially on the Circadian Rhythm page. I don't consider myself an artist or a designer, most of my experience involves implementing UIs created by designers.

This was built with React. It uses SCSS for the stylesheets. The graphics were made with Krita, and the chart is rendered with SVG markup.
