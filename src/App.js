import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Menu from './menu';
import Footer from './footer';
import Home from './home';
import Why from './why';
import How from './how';
import CircadianRhythm from './circadian-rhythm';
import Hacks from './hacks';
import References from './references';
import About from './about';

import './styles.scss';

function App() {
  return (
    <div className="App">
      <Router>
        <header>
          <Menu />
        </header>
        <div className="main">
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/why" component={Why} />
            <Route path="/how" component={How} />
            <Route path="/rhythm" component={CircadianRhythm} />
            <Route path="/hacks" component={Hacks} />
            <Route path="/references" component={References} />
            <Route paht="/about" component={About} />
          </Switch>
        </div>
        <Footer />
      </Router>
    </div>
  );
}

export default App;
