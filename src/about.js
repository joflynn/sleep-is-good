import React from 'react';
import { Link } from 'react-router-dom';

export default function About() {
  return (
    <div className="container about blue auto-height">
      <p>I made this to play with some modern HTML and CSS techniques, and to advocate for better sleep. I am not a doctor and none of this should be taken as medical advice.</p>

      <p>I limited myself to a weekend to build this. If I had more time, I would have put more effort into the design, especially on the <Link to="/rhythm">Circadian Rhythm</Link> page.  I don't consider myself an artist or a designer, most of my experience involves implementing UIs created by designers. </p>

      <p>This was built with React. It uses SCSS for the stylesheets.  The graphics were made with Krita, and the chart is rendered with SVG markup.</p>

      <p>Source code available at <a href="https://gitlab.com/joflynn/sleep-is-good">gitlab</a>. Hosted by AWS.</p>

      <p>Thanks for checking it out.</p>

      <div className="right">MIT License<br />Copyright &copy; 2019 Joe Flynn</div>
    </div>
  );
}
