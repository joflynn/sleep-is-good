import React, { useEffect, useReducer } from 'react';
import { Link } from 'react-router-dom';

import Slider from './slider';
import Page from './page';

export default function CircadianRhythm() {

  const INCREMENT_PAGE = "INCREMENT_PAGE";
  const DECREMENT_PAGE = "DECREMENT_PAGE";

  const PAGE_COUNT = 5;

  const [state, dispatch] = useReducer((state, action) => {
    const { currentPage } = state;
    let hideTransition;
    switch (action.type) {
      case INCREMENT_PAGE:
        hideTransition = (currentPage + 1 === PAGE_COUNT) ? true : false;
        return {currentPage: ((currentPage + 1) % PAGE_COUNT), hideTransition};

      case DECREMENT_PAGE:
        hideTransition = (currentPage === 0) ? true : false;
        return {...state, currentPage: (currentPage - 1 + PAGE_COUNT) % PAGE_COUNT, hideTransition};

      default:
        return state;
    }

  }, {currentPage: 0, hideTransition: false});

  useEffect(() => {

    function onWheel(event) {
      if (event.deltaY > 0) {
        increment();
      } else {
        decrement();
      }
    }

    function onKeyDown(event) {
      switch (event.keyCode) {
        case 13: //enter
        case 32: //space
        case 39: //right
        case 40: //down
          increment();
          break;

        case 37: //left
        case 38: //up
          decrement();
          break;


        default:
          break;
      }
    }
    function onTouch(event) {
      increment();
    }

    function increment() {
      if (state.currentPage + 1 === PAGE_COUNT) {
        setTimeout(() => dispatch({type: INCREMENT_PAGE}), 10);
      }
      dispatch({type: INCREMENT_PAGE});
    }

    function decrement() {
      dispatch({type: DECREMENT_PAGE});
      if (state.currentPage === 0) {
        setTimeout(() => dispatch({type: DECREMENT_PAGE}), 10);
      }
    }

    window.addEventListener('wheel', onWheel);
    window.addEventListener('keydown', onKeyDown);
    window.addEventListener('touchend', onTouch);
    return () => {
      window.removeEventListener('wheel', onWheel);
      window.removeEventListener('keydown', onKeyDown);
      window.removeEventListener('touchend', onTouch);
    };
  }, [state.currentPage]);


  const { currentPage, hideTransition } = state;
  return (
    <div> 
      <Slider page={currentPage} hideTransition={hideTransition}> 
        <Page className="morning">
          <label className="top-right">Circadian Rhythm is the name of the myriad biological processes that vary over the course of a day.</label>
          <label className="mid-right">Among them are the levels of the hormones cortisol and melatonin.</label>
        </Page>
        <Page className="noon">
          <label className="mid-right">The best way to regulate this cycle is to live in sync with the Sun.</label>
        </Page>
        <Page className="evening">
          <label className="mid-right">You should be exposed to blue light during the day, either from sunlight or artificial sources.</label>
        </Page>
        <Page className="night">
          <label className="top-right">Then limit blue light during the evening and at night. <Link to="/how">Here are some other ideas</Link></label>
        </Page>
        <Page className="morning">
          <label className="top-right">Circadian Rhythm is the name of the myriad biological processes that vary over the course of a day.</label>
          <label className="mid-right">Among them are the levels of the hormones cortisol and melatonin.</label>
        </Page>
      </Slider>
    </div>
  );
}
