import React from 'react';
import { Link } from 'react-router-dom';

export default function Footer() {

  return (
    <footer>
      <div>
        <Link to="/about">About</Link>
      </div>
    </footer>
  );
}
