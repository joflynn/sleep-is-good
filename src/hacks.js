import React from 'react';
import { Link } from 'react-router-dom';
export default function Hacks() {

  return (
    <div className="container hacks purple">
      <div className="p5">
        <h2>Sleep Hacks</h2>
        <ul>
          <li>Temperature is a signal for the <Link to="/rhythm">circadian clock</Link>, keep your bedroom a few degrees colder than the rest of your home.</li>
          <li>Use a white noise generator to mask noises you can't control.</li>
          <li>Ditch the alarm clock, if you can. Try replacing an audible alarm with a <a href="https://www.google.com/search?q=light+alarm+clock">light alarm clock</a>.</li>
          <li>Don't use a screen before bed. If you must, be sure to use a blue light filter like <a href="https://justgetflux.com/">f.lux</a> or <a href="https://www.google.com/search?q=amber+tinted+glasses">amber tinted glasses</a>.</li>
          <li>Limit food in the evening.  Starchy veggies at dinnertime may be helpful for sleep.</li>
          <li>Limit caffiene to the morning!</li>
        </ul>
      </div>
    </div>
  );
}
