import React from 'react';
import { Link } from 'react-router-dom';

import eight from './eight.png';
import balance from './balance.png';

export default function Home() {

  return (
    <div className="container home">
      <div className="panels">
        <div className="panel yellow">
          <div className="p5">
            <h1>Sleep is Good</h1>
            <p>You should probably get more of it</p>
          </div>
        </div>
        <div className="panel red">
          <div className="p5">
            <h2>tl;dr;</h2>
            <ul>
              <li>Spend time outside during the day or use a light therapy box for an hour during the day.</li>
              <li>Wear amber tinted glasses for the last few hours before bedtime.</li>
              <li>Go to bed at the same time every day and keep your bedroom cool and dark.</li>
            </ul>
          </div>
        </div>
        <div className="panel">
          <div className="p5">
            <p><Link to="/why">2/3 of Americans undersleep. Lack of sleep is a contributing factor to chronic disease.</Link></p>
          </div>
        </div>
        <div className="panel blue">
          <div className="p5 center">
            <p><Link to="/how">Try to get 8 hours of sleep every night.</Link></p>
            <img src={eight} alt="Eight Hours a night" />
          </div>
        </div>
        <div className="panel purple">
          <div className="p5 center">
            <p><Link to="/hacks">Unlike many other systems (like metabolism) the body does not have a credit/debit system for sleep. You can't catch up on sleep.</Link></p>
            <img src={balance} alt="You can't make up lost sleep" />
          </div>
        </div>
      </div>
    </div>
  );
}
