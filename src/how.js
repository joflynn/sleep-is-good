import React from 'react';

export default function How() {

  return (
    <div className="container how">
      <div className="panels">
        <div className="panel center red"> 
          <h2>How to improve sleep?</h2>
        </div>
        <div className="panel center blue">
          <blockquote>&ldquo;The best thing you can do to regulate your circadian clock is to live in tune with the sun.&rdquo;<br />-Sarah Ballantyne</blockquote>
        </div>
        <div className="panel yellow">
          <div className="p5">
            <dl>
              <dt>Blue light during the day</dt>
              <dd>One of the best ways to set your circadian clock is to be exposed to bright light (ideally sunlight) during the day. It is the blue light contained in sunlight that triggers this signal. If you can't get enough sunlight, a light therapy box can help support melatonin production.</dd>
              <dt>Dim red light at night</dt>
              <dd>Likewise it's important to limit light in general, and blue light in particular, in the evening. Glasses with yellow lenses, and blue-light limiting software can help with this.</dd>
            </dl>
          </div>
        </div>
        <div className="panel center purple">
          <blockquote>
            &ldquo;You probably recognize cortisol as being the master stress hormone. It's also a very imporant circadian rhythm hormone. This means if you're under stress, not only do you have all the effects of elevated and disregulated cortisol to deal with, but you also disrupt your circadian rhythms.&rdquo;<br />-Sarah Ballantyne
          </blockquote>
        </div>
        <div className="panel yellow">
          <div className="p5">
            <dl>
              <dt>Have a Bedtime</dt>
              <dd>It's important to be consistent. Even on the weekends. Fluctuations can disrupt the circadian rhythm.</dd>
              <dt>Cool Dark Room</dt>
              <dd>Ambient temperature is a cue for the circadian clock. Your bedroom should be cool, ideally 18C (65°F) or lower.</dd>
              <dt>Eat Organ Meats and Seafood</dt>
              <dd>Melatonin is made from serotonin, which is made from tryptophan. Organ meats, like liver, and seafood contain a higher ratio of tryptophan to other aminos.  The omega-3 fats from seafood also help the brain be resiliant to stressors.</dd>
              <dt>Vary with the season</dt>
              <dd>It is normal to get a little less sleep in the summer, and a little more sleep in the winter. Aim for 7-8 hours in the summer, and 9-10 in the winter.</dd>
            </dl>
          </div>
        </div>
      </div>
    </div>
  );
}
