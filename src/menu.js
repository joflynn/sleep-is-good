import React, { useState } from 'react';
import {Link} from 'react-router-dom';

export default function Menu() {

  const [collapsed, setCollapsed] = useState(true);
  const collapsedClass = collapsed ? "collapsed" : "";

  function toggleMenu() {
    setCollapsed(!collapsed);
  }

  function closeMenu() {
    setCollapsed(true);
  }

  return (
    <nav className="menu">
      <h1><Link to="/">Sleep is good</Link></h1>
      <div className="collapse" onClick={toggleMenu}>Menu</div>
      <ul className={collapsedClass}>
        <li><Link onClick={closeMenu} to="/why">Why?</Link></li>
        <li><Link onClick={closeMenu} to="/how">How?</Link></li>
        <li><Link onClick={closeMenu} to="/rhythm">Circadian Rhythm</Link></li>
        <li><Link onClick={closeMenu} to="/hacks">Hacks</Link></li>
        <li><Link onClick={closeMenu} to="/references">References</Link></li>
      </ul>
    </nav>
  );
}
