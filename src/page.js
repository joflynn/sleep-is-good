import React from 'react';

import sun from './sun.png';
import moon from './moon.png';

export default function Page(props) {

  const time = props.className;

  const classes = `page ${props.className}`;

  const object = (time === "night") ? "moon" : "sun";
  const objectImg = (object === "moon") ? moon : sun;


  const data = {
    "morning": {
      "melatonin": [32, 35, 38, 40, 45, 50, 62, 74, 80, 82, 85, 86, 87, 88, 88, 89, 89],
      "cortisol":  [90, 89, 87, 85, 82, 80, 75, 72, 70, 62, 55, 50, 46, 38, 34, 33, 32]
    },
    "noon": {
      "melatonin": [89, 89, 89, 90, 90, 90, 90, 90, 90, 91, 91, 91, 91, 91, 90, 90, 90],
      "cortisol":  [32, 34, 36, 39, 41, 43, 45, 50, 55, 59, 62, 67, 70, 73, 78, 80, 82],
    },
    "evening": {
      "melatonin": [90, 89, 89, 88, 87, 86, 86, 85, 84, 84, 83, 82, 82, 80, 74, 72, 70],
      "cortisol":  [82, 82, 83, 83, 84, 84, 85, 85, 86, 86, 87, 88, 89, 90, 91, 92, 93],
    },
    "night": {
      "melatonin": [70, 62, 55, 48, 42, 40, 38, 36, 34, 32, 31, 31, 30, 30, 31, 31, 32],
      "cortisol":  [93, 94, 96, 96, 95, 95, 94, 94, 94, 93, 93, 93, 92, 92, 91, 90, 90],
    },
  }

  const melatonin = buildLine(data[time]["melatonin"]);
  const cortisol = buildLine(data[time]["cortisol"]);

  function buildLine(values) {
    let prev = 0;
    let dx = 100 / (values.length - 1);
    return values.map((cur, idx) => {
      if (prev === 0) {
        prev = cur;
        return null;
      }
      const x1 = (idx-1)*dx + "%";
      const x2 = (idx*dx + 0.25) + "%";
      const y1 = prev + "%";
      const y2 = cur + "%";
      prev = cur;
      return <line key={idx} x1={x1} y1={y1} x2={x2} y2={y2} />

    });
  }

  return (
    <div className={classes}>
      <div className="chart">
        <svg>
          <g className="line melatonin">
            {melatonin}
          </g>
          <g className="line cortisol">
            {cortisol}
          </g>
        </svg>
        <label className="melatonin">Melatonin</label>
        <label className="cortisol">Cortisol</label>
        {props.children}
      </div>
      <div className="scene">
        <div className="sky">
          <img src={objectImg} className={object} alt={object} />
        </div>
        <div className="horizon">
        </div>
      </div>
    </div>
  );
}
