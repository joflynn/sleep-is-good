import React from 'react';

export default function References() {
  return (
    <div className="container references purple">
      <div className="p5">
        <h2>Don't take my word for it &hellip;</h2>

        <ol>
          <li> 
            Matt Walker | TED 2019 <strong>Sleep is your superpower</strong>
            <div><div><iframe title="Embedded Video" src="https://embed.ted.com/talks/lang/en/matt_walker_sleep_is_your_superpower" frameborder="0" scrolling="no" allowfullscreen></iframe></div></div>
          </li>
          <li>
            <div className="mla">
              Ballantyne, Sarah. <em>Paleo principles: the science behind the paleo template, step-by-step guides, meal plans, and 200+ healthy & delicious recipes for real life.</em> Las Vegas, NV: Victory Belt Publishing, Inc, 2017. Print.
            </div>
          </li>
        </ol>
      </div>
    </div>
  );
}
