import React from 'react';

export default function Slider(props) {

  const { children, page, hideTransition } = props;

  const style = {
    transform: "translate(" + (-100 * page) + "vw, 0)"
  };

  const className = "slider " + (hideTransition ? "hide-transition" : "");

  return (
    <div className="slider-container">
      <div className={className} style={style}>
        {children}
      </div>
    </div>
  );
}
