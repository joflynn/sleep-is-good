import React from 'react';

export default function Why() {

  return (
    <div className="container why">
      <div className="panels">
        <div className="panel blue center">
          <h2>Why does this matter?</h2>
        </div>
        <div className="panel red">
          <blockquote>&ldquo;You need sleep after learning to essentially hit the save button on new memories. Recently we discovered that you also need sleep before learning, to actually prepare your brain like a dry sponge ready to soak up new memory.&rdquo;<br /> - Matt Walker</blockquote>
        </div>
        <div className="panel purple">
          <blockquote>&ldquo;&hellip;sleeping less than 6 hours per night increased the risk of all-cause mortality by 12 percent.&rdquo;<br />- Sarah Ballantyne</blockquote>
        </div>
        <div className="panel parallax me-sleeping">
        </div>
        <div className="panel white">
          <blockquote>&ldquo;During a week, variability in bedtimes of greater than 2 hours increases the risk of obesity by 14 percent.&rdquo;<br />- Sarah Ballantyne</blockquote>
        </div>
        <div className="panel yellow">
          <blockquote>&ldquo;There is a global expirement performed on 1.6 billion people across 70 countries, twice a year. It's called <strong>Daylight Savings Time</strong>. In the spring when we lose one hour of sleep we see a subsequent <strong>24% increase in heart attacks</strong> that following day. In the autumn, when we gain an hour, we see a 21% reduction in the number of heart attacks.&rdquo;<br /> - Matt Walker</blockquote>
        </div>
      </div>
    </div>
  );
}
